#ifndef _CENTITY_H_
#define _CENTITY_H_

#include <vector>
#include <typeindex>

class CComponentBase;

class CEntity {
public:
	CEntity();
	CEntity(std::string);
	~CEntity();

	void setName(std::string);
	std::string getName();

	template<typename T>
	void updateComponent(T*);

	template<typename T>
	void attachComponent(T*);
	void dettachComponent(size_t);

	template<typename T>
	void dettachComponent();

	template<typename>bool	hasComponent();
	bool					hasComponent(size_t);

	template<typename T> T* getComponent();
	CComponentBase*			getComponent(size_t);

private:
	std::string name;
	std::vector<CComponentBase*> components;
};

template<typename T>
bool CEntity::hasComponent() {
	return (this->hasComponent(CComponent<T>::componentID));
}

template<typename T>
T* CEntity::getComponent() {
	return static_cast<T*>(this->getComponent(CComponent<T>::componentID));
}

template<typename T>
void CEntity::attachComponent(T* comp) {
	if(components.size() <= comp->componentID) components.resize(CComponentBase::total_components);

	if(hasComponent(comp->componentID)) this->components[comp->componentID]->entity = NULL;
	this->components[comp->componentID] = comp;
}

template<typename T>
void CEntity::dettachComponent() {
	this->dettachComponent(T::componentID);
}

// FOR INTERN USE ONLY.
template<typename T>
void CEntity::updateComponent(T* comp) {
	this->components[comp->componentID] = comp;
}

#endif // _CENTITY_H_