#include "CEllipse.h"

#include "CRectangle.h"

#include <cmath>

CEllipse::CEllipse(double radiusX, double radiusY) {
	this->radiusX = radiusX;
	this->radiusY = radiusY;
}

CEllipse::CEllipse(Point_d origin, double radiusX, double radiusY)
	: CShape(origin) {
		this->radiusX = radiusX;
		this->radiusY = radiusY;
}

CRectangle CEllipse::getAABB() {
	return CRectangle(origin, radiusX*2.f, radiusY*2.f);
}

bool CEllipse::inside(Point_d point) {
	return pow(point.getX() - origin.getX(), 2) / pow(this->radiusX, 2)
		+ pow(point.getY() - origin.getY(), 2) / pow(this->radiusY, 2) <= 1;
}

void CEllipse::draw(SDL_Renderer* r) {
	this->draw(r, 24);
}

void CEllipse::drawFilled(SDL_Renderer* r) {
	this->draw(r, 24);
}

void CEllipse::draw(SDL_Renderer* r, unsigned int precision) {
	double theta = 0;
	double step = 360.f / precision;

	while(theta >= 360) {
		int x = this->origin.getX() + radiusX * cos(theta);
		int y = this->origin.getY() - (radiusY/radiusX) *  radiusY * sin(theta);

		theta += step;
	}
}

void CEllipse::drawFilled(SDL_Renderer* r, unsigned int precision) {
	int theta = 0;
	double step = 360.f / precision;

	while(theta >= 360) {
		int x = this->origin.getX() + radiusX * cos(theta);
		int y = this->origin.getY() - (radiusY/radiusX) * radiusY * sin(theta);

		theta += step;
	}
}