#ifndef _CSYSINPUT_H_
#define _CSYSINPUT_H_

#include "CSystem.h"
#include "CCompInput.h"

#include "SDLEvent.h"

class CSysInput : public CSystem {
private:
	//std::vector<CEntity*> hovers;
	//int pointerX, pointerY;

public:
	void update(CEntity* entity, SDL_Event* eventHandler);
		bool updatePointer(CEntity* entity, SDL_Event* eventHandler);
		bool updateMap(CEntity* entity, SDL_Event* eventHandler);
		bool updateCamera(CEntity* entity, SDL_Event* eventHandler);

	void inputAction(CEntity* entity, SDL_Event* eventHandler);
		void inputDeselect(CEntity* entity);
		void inputSelect(CEntity* entity);
		void inputMove(CEntity* entity);
	void inputMoveCamera(CEntity* entity, SDL_Event* eventHandler);
	void inputStopCamera(CEntity* entity, SDL_Event* eventHandler);
	void inputMap(CEntity* entity, SDL_Event* eventHandler);
	void inputHover(CEntity* entity, SDL_Event* eventHandler);
		void inputHoverOn(CEntity* entity);
		void inputHoverOff(CEntity* entity);
	void inputPointer(CEntity* entity, SDL_Event* eventHandler);
};

#endif //_CSYSINPUT_H_