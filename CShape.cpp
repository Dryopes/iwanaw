#include "CShape.h"
#include "CRectangle.h"

#include "SDL2_gfxPrimitives.h"

CShape::CShape() {
}

CShape::CShape(Point_d origin) {
	this->origin = origin;
}

CShape::CShape(double x, double y) {
	this->origin.setPosition(x, y);
}

CShape::~CShape() {
}

void CShape::setOrigin(Point_d origin) {
	this->origin = origin;
}

Point_d CShape::getOrigin() {
	return this->origin;
}

SDL_Texture* CShape::toTexture(SDL_Renderer* r) {
	CRectangle aabb = this->getAABB();
	SDL_Texture* tex = SDL_CreateTexture(r, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, aabb.getWidth() + 1, aabb.getHeight() + 1);
	SDL_SetTextureBlendMode(tex, SDL_BLENDMODE_BLEND);
	
	SDL_SetRenderTarget(r, tex);
	SDL_SetRenderDrawBlendMode(r, SDL_BLENDMODE_NONE);

	SDL_SetRenderDrawColor(r, 255, 255, 255, 0);
	SDL_RenderFillRect(r, NULL);

	SDL_SetRenderDrawColor(r, 0, 0, 0, 255);
	this->draw(r, true);

	SDL_SetRenderTarget(r, NULL);
	SDL_SetRenderDrawBlendMode(r, SDL_BLENDMODE_BLEND);

	return tex;
}

SDL_Texture* CShape::toTextureFilled(SDL_Renderer* r) {
	CRectangle aabb = this->getAABB();
	SDL_Texture* tex = SDL_CreateTexture(r, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, aabb.getWidth() + 1, aabb.getHeight() + 1);
	SDL_SetTextureBlendMode(tex, SDL_BLENDMODE_BLEND);
	
	SDL_SetRenderTarget(r, tex);
	SDL_SetRenderDrawBlendMode(r, SDL_BLENDMODE_NONE);

	SDL_SetRenderDrawColor(r, 255, 255, 255, 0);
	SDL_RenderFillRect(r, NULL);

	SDL_SetRenderDrawColor(r, 0, 0, 255, 128);
	this->drawFilled(r, true);

	SDL_SetRenderTarget(r, NULL);
	SDL_SetRenderDrawBlendMode(r, SDL_BLENDMODE_BLEND);

	return tex;
}