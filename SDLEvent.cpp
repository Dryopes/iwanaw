#include "SDLEvent.h"

SDLEvent::SDLEvent() {
}

SDLEvent::~SDLEvent() {

}

bool SDLEvent::poll() {
	SDL_zero(this->handler);
	return SDL_PollEvent(&this->handler);
}

void SDLEvent::toIntent() {
	SDL_Event intent;
	SDL_zero(intent);
	intent.type = 0;
	intent.user.code = 0;
	intent.user.data1 = NULL;
	intent.user.data2 = NULL;

	switch(this->handler.type) {
		/*
		 * MOUSE
		 */

		case SDL_MOUSEBUTTONDOWN: {
			std::cout << "MouseButtonDown event!" << std::endl;
			break;
		}
		case SDL_MOUSEBUTTONUP: {
			//std::cout << "MouseButtonUp event!" << std::endl;
			intent.type = SDL_USEREVENT;
			intent.user.type = (Uint32)INTENT::POINTER;
			switch(handler.button.button) {
				case SDL_BUTTON_LEFT: intent.user.code = (Uint32)POINTER::ACTION1; break;
				case SDL_BUTTON_RIGHT: intent.user.code = (Uint32)POINTER::ACTION2; break;
				case SDL_BUTTON_MIDDLE: intent.user.code = (Uint32)POINTER::ACTION3; break;
				default: intent.user.code = (Uint32)POINTER::NONE; break;
			}
			break;
		}
		case SDL_MOUSEMOTION: {
			intent.type = SDL_USEREVENT;
			intent.user.type = (Uint32)INTENT::POINTER;
			intent.user.code = (Uint32)POINTER::SET;
			intent.user.data1 = (void*)handler.motion.x;
			intent.user.data2 = (void*)handler.motion.y;
			break;
		}
		case SDL_MOUSEWHEEL: {
			std::cout << "MouseWheel Event!" << std::endl;
		}

		/*
		 * KEYBOARD
		 */

		case SDL_KEYDOWN: {
			if(handler.key.repeat == 0) {
				switch(handler.key.keysym.scancode) {
					case SDL_SCANCODE_UP: {
						intent.type = SDL_USEREVENT;
						intent.user.type = (Uint32)INTENT::MOVECAMERA;
						intent.user.code = (Uint32)CAMERACODE::NORTH;
						break;
					}
					case SDL_SCANCODE_RIGHT: {
						intent.type = SDL_USEREVENT;
						intent.user.type = (Uint32)INTENT::MOVECAMERA;
						intent.user.code = (Uint32)CAMERACODE::EAST;
						break;
					}
					case SDL_SCANCODE_DOWN: {
						intent.type = SDL_USEREVENT;
						intent.user.type = (Uint32)INTENT::MOVECAMERA;
						intent.user.code = (Uint32)CAMERACODE::SOUTH;
						break;
					}
					case SDL_SCANCODE_LEFT: {
						intent.type = SDL_USEREVENT;
						intent.user.type = (Uint32)INTENT::MOVECAMERA;
						intent.user.code = (Uint32)CAMERACODE::WEST;
						break;
					}
				}
			}
			std::cout << "KeyDown event!" << std::endl;
			break;
		}
		case SDL_KEYUP: {
			if(handler.key.repeat == 0) {
				switch(handler.key.keysym.scancode) {
					case SDL_SCANCODE_UP: {
						intent.type = SDL_USEREVENT;
						intent.user.type = (Uint32)INTENT::STOPCAMERA;
						intent.user.code = (Uint32)CAMERACODE::NORTH;
						break;
					}
					case SDL_SCANCODE_RIGHT: {
						intent.type = SDL_USEREVENT;
						intent.user.type = (Uint32)INTENT::STOPCAMERA;
						intent.user.code = (Uint32)CAMERACODE::EAST;
						break;
					}
					case SDL_SCANCODE_DOWN: {
						intent.type = SDL_USEREVENT;
						intent.user.type = (Uint32)INTENT::STOPCAMERA;
						intent.user.code = (Uint32)CAMERACODE::SOUTH;
						break;
					}
					case SDL_SCANCODE_LEFT: {
						intent.type = SDL_USEREVENT;
						intent.user.type = (Uint32)INTENT::STOPCAMERA;
						intent.user.code = (Uint32)CAMERACODE::WEST;
						break;
					}
				}
			}
			std::cout << "KeyUp event!" << std::endl;
			break;
		}

		case SDL_TEXTEDITING: {
			std::cout << "TextEditing Event!" <<std::endl;
			break;
		}
		case SDL_TEXTINPUT: {
			std::cout << "TextInput Event!" << std::endl;
			break;
		}

		/*
		 * JOYSTICK
		 */

		case SDL_JOYAXISMOTION: {
			std::cout << "JoyAxisMotion Event! Axis: " << (int)handler.jaxis.axis << ", Value: " << handler.jaxis.value << std::endl;
			if(handler.jaxis.axis == 2 && handler.jaxis.value > 10000) {
				intent.type = SDL_USEREVENT;
				intent.user.type = (Uint32)INTENT::MOVECAMERA;
				intent.user.code = (Uint32)CAMERACODE::EAST;
			}else if(handler.jaxis.axis == 2 && handler.jaxis.value < -10000) {
				intent.type = SDL_USEREVENT;
				intent.user.type = (Uint32)INTENT::MOVECAMERA;
				intent.user.code = (Uint32)CAMERACODE::WEST;
			}else if(handler.jaxis.axis == 2) {
				intent.type = SDL_USEREVENT;
				intent.user.type = (Uint32)INTENT::STOPCAMERA;
				intent.user.code = (Uint32)CAMERACODE::WEST;
			}

			if(handler.jaxis.axis == 3 && handler.jaxis.value > 10000) {
				intent.type = SDL_USEREVENT;
				intent.user.type = (Uint32)INTENT::MOVECAMERA;
				intent.user.code = (Uint32)CAMERACODE::SOUTH;
			}else if(handler.jaxis.axis == 3 && handler.jaxis.value < -10000) {
				intent.type = SDL_USEREVENT;
				intent.user.type = (Uint32)INTENT::MOVECAMERA;
				intent.user.code = (Uint32)CAMERACODE::NORTH;
			}else if(handler.jaxis.axis == 3) {
				intent.type = SDL_USEREVENT;
				intent.user.type = (Uint32)INTENT::STOPCAMERA;
				intent.user.code = (Uint32)CAMERACODE::NORTH;
			}
			break;
		}

		case SDL_JOYBALLMOTION: {
			std::cout << "JoyBallMotion Event!" << std::endl;
			break;
		}

		case SDL_JOYHATMOTION: {
			std::cout << "JoyHatMotion Event!" << std::endl;
			break;
		}

		case SDL_JOYBUTTONDOWN: {
			std::cout << "JoyButtonDown Event!" << std::endl;
			break;
		}
		case SDL_JOYBUTTONUP: {
			std::cout << "JoyButtonUp Event!" << std::endl;
			break;
		}

		case SDL_JOYDEVICEADDED: {
			std::cout << "JoyDeviceAdded Event!" << std::endl;
			SDL_Joystick* joy = SDL_JoystickOpen(this->handler.jdevice.which);
			if(joy) {
				std::cout << "Opened Joystick " << this->handler.jdevice.which << std::endl;
				std::cout << "Name: " << SDL_JoystickNameForIndex(this->handler.jdevice.which) << std::endl;
				std::cout << "Number of Axes: " << SDL_JoystickNumAxes(joy) << std::endl;
				std::cout << "Number of Buttons: " << SDL_JoystickNumButtons(joy) << std::endl;
				std::cout << "Number of Balls: " << SDL_JoystickNumBalls(joy) << std::endl;
			}
			break;
		}
		case SDL_JOYDEVICEREMOVED: {
			std::cout << "JoyDeviceRemoved Event!" << std::endl;
			break;
		}

		/*
		 * CONTROLLER
		 */
		case SDL_CONTROLLERBUTTONDOWN: {
			std::cout << "ControllerButtonDown Event! " << std::endl;
			break;
		}

		case SDL_CONTROLLERDEVICEADDED: {
			std::cout << "ControllerDeviceAdded Event!" << std::endl;
			break;
		}
		case SDL_CONTROLLERDEVICEREMOVED: {
			std::cout << "ControllerDeviceRemoved Event!" << std::endl;
			break;
		}
		case SDL_CONTROLLERDEVICEREMAPPED: {
			std::cout << "ControllerDeviceRemapped Event!" << std::endl;
			break;
		}
		default: {
			std::cout << "EVENT!: " << this->handler.type <<std::endl;
			break;
		}
	}

	if(intent.type != 0) {
		SDL_PushEvent(&intent);
	}
}

SDL_Event* SDLEvent::get() {
	return &this->handler;
}