#include "CSysPathFinding.h"

#include "CCompMapped.h"
#include "CCompMap.h"
#include "CCompTile.h"

#include <iostream>

void CSysPathFinding::update(CEntity* entity) {
	CCompMapped* mapped = entity->getComponent<CCompMapped>();
	
	unvisited.push(Node(mapped->getTilePos(), mapped->getTileSpeed()));

	if(mapped->getTileSpeed() > 0) {
		while(!unvisited.empty()) {
			if(!inVisited(unvisited.top())) {
				getNeightbours(mapped);
				visited.push_back(unvisited.top());
			}
			unvisited.pop();
		}
	}

	std::vector<std::vector<unsigned int>> paths;
	paths.resize(visited.size());

	std::cout << "Reachables: " << std::endl;
	unsigned int index = 0;
	while(!visited.empty()) {
		paths[index] = visited.back().path;
		paths[index].push_back(visited.back().position);

		std::cout << visited.back().position << ", " << visited.back().weight;
		std::cout << "| Path: ";
		for(int i: visited.back().path) std::cout << i << ", ";
		std::cout << std::endl;
		visited.pop_back();

		++index;
	}

	mapped->setPaths(paths);
}

void CSysPathFinding::getNeightbours(CCompMapped* mapped) {
	CCompMap* map = mapped->getMap()->getComponent<CCompMap>();
	Node node = unvisited.top();

	std::queue<Node> result;

	Point_i32 point(node.position%map->width, node.position/map->width);

	int align = point.getY()%2;

	Point_i32 point1(point.getX() + align - 1,	point.getY() - 1);
	Point_i32 point2(point.getX() + align,		point.getY() - 1);

	Point_i32 point3(point.getX() - 1,			point.getY());
	Point_i32 point4(point.getX() + 1,			point.getY());

	Point_i32 point5(point.getX() + align - 1,	point.getY() + 1);
	Point_i32 point6(point.getX() + align,		point.getY() + 1);

	insertNode(map, point1);
	insertNode(map, point2);
	insertNode(map, point3);
	insertNode(map, point4);
	insertNode(map, point5);
	insertNode(map, point6);
}

void CSysPathFinding::insertNode(CCompMap* map, Point_i32& position) {

	if(position.getX() >= 0 && position.getX() < map->width &&
		position.getY() >= 0 && position.getY() < map->height) {
			int tile = position.getX() + position.getY() * map->width;
			int weight = unvisited.top().weight - map->tiles[tile].getComponent<CCompTile>()->cost;

			Node node(tile, weight);
			node.path.insert(node.path.begin(), unvisited.top().path.begin(), unvisited.top().path.end());
			node.path.push_back(unvisited.top().position);

			if(weight >= 0) unvisited.push(node);
	}
}

bool CSysPathFinding::inVisited(Node& node) {
	return std::find(visited.begin(), visited.end(), node) != visited.end();
}