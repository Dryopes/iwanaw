#ifndef _CCOMPPATH_H_
#define _CCOMPPATH_H_

#include "CComponent.h"
#include "CShape.h"

class CCompPath : public CComponent<CCompPath> {
public:
	std::vector<Point_i32> path;
};

#endif //_CCOMPPATH_H_