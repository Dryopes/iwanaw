#ifndef _CSYSRENDER_H_
#define _CSYSRENDER_H_

#include "CSystem.h"

#include "SDLRenderer.h"

class CSysRender : public CSystem {
public:
	CSysRender(SDLRenderer* renderer);
	~CSysRender();

	void update(CEntity* entity);
	void update(CEntity* entity, long offset_x, long offset_y);

	SDLRenderer* getRenderer();

private:
	SDLRenderer* renderer;
};

#endif //_CSYSRENDER_H_