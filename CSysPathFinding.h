#ifndef _CPATHFINDING_H_
#define _CPATHFINDING_H_

#include "CSystem.h"

#include "CCompMapped.h"
#include "CCompMap.h"

#include <queue>

class Node {
public:
	Node(int position, double weight) {
		this->position = position;
		this->weight = weight;
	}

	std::vector<unsigned int> path;
	int position;
	double weight;

};
inline bool operator==(const Node& l, const Node& r) {return l.position == r.position;}
inline bool operator<(const Node& l, const Node& r) {return l.weight < r.weight;}

class CSysPathFinding : CSystem {
public:
	void update(CEntity* entity);
	void getNeightbours(CCompMapped* mapped);
	void insertNode(CCompMap* map, Point_i32& position);

	bool inVisited(Node& node);
	void updateUnvisited(Node& node);
private:

	std::priority_queue<Node> unvisited;
	std::vector<Node> visited;
};

#endif //_CPATHFINDING_H_