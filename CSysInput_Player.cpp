#include "CSysInput.h"

#include "CSysPathFinding.h"

#include "CCompPlayer.h"
#include "CCompActor.h"
#include "CCompPlayerControlled.h"
#include "CCompMovement.h"
#include "CCompPosition.h"
#include "CCompMapped.h"
#include "CCompMap.h"

#include "CCompCamera.h"
#include "CCompCameraControlled.h"
;
void CSysInput::inputDeselect(CEntity* entity) {
	CCompPlayer* player = entity->getComponent<CCompPlayer>();

	CEntity* entitySelected = player->selectedEntity;

	CCompActor* act = (entitySelected == NULL) ? NULL : entitySelected->getComponent<CCompActor>();
	if(act != NULL) {
		act->actor.removeTag("select");
	}

	CCompMapped* mapped = (entitySelected == NULL) ? NULL : entitySelected->getComponent<CCompMapped>();
	if(mapped != NULL) {
		mapped->removePaths();
	}

	player->selectedEntity = NULL;
}

void CSysInput::inputSelect(CEntity* entity) {
	if(!entity->hasComponent<CCompPlayerControlled>()) return;

	CCompPlayer* player = entity->getComponent<CCompPlayerControlled>()
			->player->getComponent<CCompPlayer>();

	player->selectedEntity = entity;
	CCompActor* act = entity->getComponent<CCompActor>();
	CCompMapped* mapped = entity->getComponent<CCompMapped>();
	CCompMovement* mov = entity->getComponent<CCompMovement>();

	if(mov != NULL) {
		if(mov->velocityX || mov->velocityY) return;
	}

	if(act != NULL)
		act->actor.addTag("select");

	if(mapped != NULL) {
		CSysPathFinding wub;
		wub.update(entity);
	}
}

void CSysInput::inputMove(CEntity* entity) {
	CEntity* ent = entity->getComponent<CCompPlayerControlled>()->player;
	CCompPosition* pos = entity->getComponent<CCompPosition>();
	CCompPlayer* player = ent->getComponent<CCompPlayer>();

	if(player && player->selectedEntity) {
		CCompMovement* mov = player->selectedEntity->getComponent<CCompMovement>();

		if(mov != NULL) {
			CCompMapped* mapped = player->selectedEntity->getComponent<CCompMapped>();

			if(mapped == NULL) {
				mov->nextX = pos->getX();
				mov->nextY = pos->getY();
			}
			else {
				CCompMap* map = mapped->getMap()->getComponent<CCompMap>();
				CCompPosition* cam = map->entity->getComponent<CCompCameraControlled>()->camera->getComponent<CCompPosition>();

				int tile = map->getTile(pos->getX() + cam->getX(), pos->getY() + cam->getY());


				std::vector<unsigned int> paths = mapped->getPaths().getPositions();

				for(unsigned int i = 0; i < paths.size(); ++i) {
					std::cout << paths[i] << std::endl;
				}

				if(std::find(paths.begin(), paths.end(), tile) != paths.end()) {
					Point_d point = map->getTilePos(tile);
					mov->nextX = point.getX();
					mov->nextY = point.getY();

					inputDeselect(player->entity);
				}
			}
		}
	}
}