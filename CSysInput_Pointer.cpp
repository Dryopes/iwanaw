#include "CSysInput.h"

#include "CCompPointer.h"
#include "CCompPosition.h"
#include "CCompPlayerControlled.h"

#include "CCompCollision.h"
#include "CCompMapped.h"
#include "CCompCameraControlled.h"
#include "CCompActor.h"

bool CSysInput::updatePointer(CEntity* entity, SDL_Event* eventHandler) {
	return false;
}

void CSysInput::inputPointer(CEntity* entity, SDL_Event* eventHandler) {
	CCompPointer* point = entity->getComponent<CCompPointer>();
	CCompPosition* pos = entity ->getComponent<CCompPosition>();
	CCompPlayerControlled* con = entity->getComponent<CCompPlayerControlled>();

	if(point == NULL || pos == NULL) return;

	double oldX = pos->getX();
	double oldY = pos->getY();

	if(eventHandler->user.type == (Uint32)INTENT::POINTER) {
		if(eventHandler->user.code == (Uint32)POINTER::SET) {
			pos->setPos((int)eventHandler->user.data1, (int)eventHandler->user.data2);
		}
	}

	if(eventHandler->user.type == (Uint32)INTENT::POINTER) {
		if(eventHandler->user.code == (Uint32)POINTER::ACTION1) {
			if(con) inputDeselect(con->player);
			for(CEntity* entity : point->hovers) {
				inputSelect(entity);
			}
		}
		else if(eventHandler->user.code == (Uint32)POINTER::ACTION2) {
			inputMove(entity);
		}
	}

	if(oldX != pos->getX() || oldY != pos->getY()) {
		SDL_Event intent;
		SDL_zero(intent);
		intent.type = (Uint32)INTENT::HOVER;
		intent.user.data1 = entity;
		SDL_PushEvent(&intent);
	}
}

void CSysInput::inputHover(CEntity* entity, SDL_Event* eventHandler) {
	CCompCollision* col = entity->getComponent<CCompCollision>();
	CCompPosition* pos = entity->getComponent<CCompPosition>();	
	if(col == NULL || pos == NULL) return;

	CCompMapped* map = entity->getComponent<CCompMapped>();
	CCompCameraControlled* cam = entity->getComponent<CCompCameraControlled>();
	if(map != NULL) {
		cam = map->getMap()->getComponent<CCompCameraControlled>();
	}

	double shapeX = pos->getX();
	double shapeY = pos->getY();

	if(cam!= NULL) {
		shapeX -= cam->camera->getComponent<CCompPosition>()->getX();
		shapeY -= cam->camera->getComponent<CCompPosition>()->getY();
	}

	col->getShape()->setOrigin(Point_d(shapeX, shapeY));

	CCompPointer* point = static_cast<CEntity*>(eventHandler->user.data1)->getComponent<CCompPointer>();
	CCompPosition* pointer_pos = static_cast<CEntity*>(eventHandler->user.data1)->getComponent<CCompPosition>();
	
	std::vector<CEntity*>::iterator it = std::find(point->hovers.begin(), point->hovers.end(), entity);
	if(it == point->hovers.end()) {
		if(col->getShape()->inside(pointer_pos->getPos())) {
			point->hovers.push_back(entity);
			inputHoverOn(entity);
		}
	}
	else {
		if(col->getShape()->inside(pointer_pos->getPos())) {
			point->hovers.erase(it);
			inputHoverOff(entity);
		}
	}
}

void CSysInput::inputHoverOn(CEntity* entity) {
	CCompActor* act = entity->getComponent<CCompActor>();

	if(act != NULL)
		act->actor.addTag("hover");
}

void CSysInput::inputHoverOff(CEntity* entity) {
	CCompActor* act = entity->getComponent<CCompActor>();

	if(act != NULL)
		act->actor.removeTag("hover");
}