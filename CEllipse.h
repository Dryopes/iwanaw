#ifndef _CELLIPSE_H_
#define _CELLIPSE_H_

#include "CShape.h"

class CEllipse : public CShape {
	CEllipse(double radiusX, double radiusY);
	CEllipse(Point_d point, double radiusX, double radiusY);

	CRectangle getAABB();

	bool inside(Point_d point);
	bool collides(CEllipse* ellipse);

	void draw(SDL_Renderer*);
	void drawFilled(SDL_Renderer*);

	void draw(SDL_Renderer*, unsigned int precision);
	void drawFilled(SDL_Renderer*, unsigned int precision);

private:
	double radiusX, radiusY;
};

#endif