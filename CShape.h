#ifndef _CSHAPE_H_
#define _CSHAPE_H_

#include "CPoint.h"

class CRectangle;

class CShape {
public:
	CShape();
	CShape(Point_d origin);
	CShape(double x, double y);
	virtual ~CShape();

	void setOrigin(Point_d origin);
	Point_d getOrigin();

	virtual CRectangle getAABB() = 0;

	virtual bool inside(Point_d point) = 0;
	//virtual bool collides(CShape* shape) = 0;

	virtual void draw(SDL_Renderer*, bool ignore_origin = false) = 0;
	virtual void drawFilled(SDL_Renderer*, bool ignore_origin = false) = 0;
	SDL_Texture* toTexture(SDL_Renderer*);
	SDL_Texture* toTextureFilled(SDL_Renderer*);

protected:
	Point_d origin;
};

#endif