#ifndef _CCOMPCOLLISION_H_
#define _CCOMPCOLLISION_H_

#include "CComponent.h"
#include "CShape.h"
;
class CCompCollision : public CComponent<CCompCollision> {
public:
	CCompCollision(){shape = NULL;}
	CCompCollision(CShape* shape){this->shape = shape;}
	~CCompCollision(){destroyShape();}


	void setShape(CShape* shape){
		destroyShape();
		this->shape = shape;
	}

	CShape* getShape(){return this->shape;}
	

private:
	CShape* shape;

private:
	void destroyShape(){
		if(shape != NULL){
			delete shape;
			shape = NULL;
		}
	}
};

#endif