#ifndef _CSYSMOVEMENT_H_
#define _CSYSMOVEMENT_H_

#include "CSystem.h"

class CSysMovement : public CSystem {
public:
	void update(CEntity* entity, float deltaTime);
	void updateMovement(CEntity* entity, float deltaTime);
		void setMovement(CEntity* entity, double delta_x, double delta_y);
	void updateVelocity(CEntity* entity, float deltaTime);
	void updatePosition(CEntity* entity, float deltaTime);
};

#endif //_CSYSMOVEMENT_H_