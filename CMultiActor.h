#ifndef _CMULTIACTOR_H_
#define _CMULTIACTOR_H_

#include "CActor.h"
#include <memory>

enum class ACTORSETTINGS {
	FOREVER,
	ONETIME,
};

class CMultiActor {
public:
	CMultiActor();
	CMultiActor(CActor* actor);
	~CMultiActor();

	void addActor(CActor* actor);
	void addTag(std::string tag);
	void removeTag(std::string tag);

private:
	std::vector<std::unique_ptr<CActor>> actors;
	std::vector<std::unique_ptr<CActor>> settings;
};


#endif