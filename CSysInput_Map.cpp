#include "CSysInput.h"

#include "CCompMap.h"
#include "CCompPointer.h"
#include "CCompPosition.h"
#include "CCompActor.h"
#include "CCompCameraControlled.h"

bool CSysInput::updateMap(CEntity* entity, SDL_Event* eventHandler) {
	return false;
}

void CSysInput::inputMap(CEntity* entity, SDL_Event* eventHandler) {
	CCompMap* map = entity->getComponent<CCompMap>();
	if(map == NULL)
		return;

	CCompPointer* point = static_cast<CEntity*>(eventHandler->user.data1)->getComponent<CCompPointer>();
	CCompPosition* pointer_pos = static_cast<CEntity*>(eventHandler->user.data1)->getComponent<CCompPosition>();

	CCompActor* act = entity->getComponent<CCompActor>();
	CCompPosition* camPos = entity->getComponent<CCompCameraControlled>()->camera->getComponent<CCompPosition>();

	int x = pointer_pos->getX() + (int)camPos->getX();
	int y = pointer_pos->getY() + (int)camPos->getY();
	int tile = map->getTile(x, y);

	for(CEntity* entity : point->hovers) {
		entity->getComponent<CCompActor>()->actor.removeTag("hover");
	}

	point->hovers.clear();

	if(tile >= 0 && tile < map->tiles.size()) {
		map->tiles[tile].getComponent<CCompActor>()->actor.addTag("hover");
		point->hovers.push_back(&map->tiles[tile]);
	}
}