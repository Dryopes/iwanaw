#include "CSysInput.h"

#include "CCompCamera.h"
#include "CCompMovement.h"

void CSysInput::inputMoveCamera(CEntity* entity, SDL_Event* eventHandler) {
	CCompCamera* cam = entity->getComponent<CCompCamera>();
	CCompMovement* move = entity->getComponent<CCompMovement>();

	if(cam != NULL && move != NULL) {
		if(eventHandler->user.code == (Sint32)CAMERACODE::NORTH) move->moveY--;
		else if(eventHandler->user.code == (Sint32)CAMERACODE::SOUTH) move->moveY++;
		else if(eventHandler->user.code == (Sint32)CAMERACODE::WEST) move->moveX--;
		else if(eventHandler->user.code == (Sint32)CAMERACODE::EAST) move->moveX++;
	}
}

void CSysInput::inputStopCamera(CEntity* entity, SDL_Event* eventHandler) {
	CCompCamera* cam = entity->getComponent<CCompCamera>();
	CCompMovement* move = entity->getComponent<CCompMovement>();

	if(cam != NULL && move != NULL) {
		if(eventHandler->user.code == (Sint32)CAMERACODE::NORTH) move->moveY++;
		else if(eventHandler->user.code == (Sint32)CAMERACODE::SOUTH) move->moveY--;
		else if(eventHandler->user.code == (Sint32)CAMERACODE::WEST) move->moveX++;
		else if(eventHandler->user.code == (Sint32)CAMERACODE::EAST) move->moveX--;
	}
}