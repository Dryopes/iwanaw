#ifndef _CCOMPMAPPED_H_
#define _CCOMPMAPPED_H_

#include "CComponent.h"
#include "CCompPosition.h"

class Paths {
private:
	std::vector<std::vector<unsigned int>> container;
public:
	Paths();
	Paths(std::vector<std::vector<unsigned int>> c);

	std::vector<unsigned int> getPositions();
	std::vector<unsigned int> getPath(unsigned int tile);
};

class CCompMapped : public CComponent<CCompMapped> {
public:
	CCompMapped();

	void setMap(CEntity* map);
	void setTilePos(unsigned int tilePos);
	void setTileSpeed(double tileSpeed);
	void setPaths(Paths paths);

	void removePaths();

	CEntity* getMap();
	unsigned int getTilePos();
	double getTileSpeed();
	Paths getPaths();

private:
	CEntity* map;
	unsigned int tilePos;
	double tileSpeed;
	Paths paths;
	
public:
	void updateTilePos();
};

#endif