#ifndef _CCOMPONENTMANAGER_H_
#define _CCOMPONENTMANAGER_H_

#include <vector>
#include <limits>
#include <functional>

class CComponentBase;

class CComponentManager {
public:
	CComponentManager();
	~CComponentManager();

	template <typename T>
	std::vector<T>* getPool() {
		unsigned int index = poolList[T::componentID];

		if(index == std::numeric_limits<unsigned int>::max()) {
			index = T::createPool();
			poolList[T::componentID] = index;
			destroyList[T::componentID] = T::destroyPool;
		}

		return &T::containers[index];
	}


private:
	std::vector<unsigned int> poolList;
	std::vector<std::function<void(unsigned int)>> destroyList;
};

#endif