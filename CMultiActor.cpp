#include "CMultiActor.h"

CMultiActor::CMultiActor() {

};

CMultiActor::CMultiActor(CActor* actor) {
	this->addActor(actor);
};


CMultiActor::~CMultiActor() {

}

void CMultiActor::addActor(CActor* actor) {
	this->actors.push_back(std::unique_ptr<CActor>(actor));
}

void CMultiActor::addTag(std::string tag) {
	for(unsigned int index = 0; index < this->actors.size(); ++index)
		actors[index]->addTag(tag);
}