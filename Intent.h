#ifndef _INTENT_H_
#define _INTENT_H_

#include <SDL.h>

enum class INTENT {
	MESSAGE = 0x8001,
	MOVE = 0x8002,
	MOVECAMERA = 0x8003,
	STOPCAMERA = 0x8004,
	HOVER = 0x8005,
	POINTER = 0x8006,
	ACTION = 0x8007,
};

enum class MESSAGE {
	CHANGED,
};

enum class POINTER {
	NONE,
	SET,
	MOVE,
	STOP,
	ACTION1,
	ACTION2,
	ACTION3,
};

enum class ACTION {
	NONE,
	FIRST,
	SECOND,
	THIRTH,
};

enum class CAMERACODE {
	NORTH,
	EAST,
	SOUTH,
	WEST
};

enum class HOVER {
	OFF,
	ON,
};


#endif //_INTENT_H_