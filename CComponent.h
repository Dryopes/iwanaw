#ifndef _COMPONENT_H_
#define _COMPONENT_H_

#include <vector>
#include <string>
#include <memory>

#include "CEntity.h"
#include "Intent.h"
#include "CComponentManager.h"


// Because we need base classes for base classes in c++
class CComponentBase {
public:
	static unsigned int total_components;
	CEntity* entity;

	virtual ~CComponentBase(){};
	virtual void destroy() = 0;

protected:
	CComponentBase(){entity = NULL;};
};

// I feel like this class is _really_ ugly.
template<typename T>
class CComponent : public CComponentBase {
public:
	virtual ~CComponent(){};

	// The vector holding the conpoments. Very cache friendly! :P
	//static std::vector<T> container;
	static unsigned int componentID;
	static std::vector<std::vector<T>> containers;
	std::vector<T> *pool;

	static unsigned int createPool() {
		containers.push_back(std::vector<T>());
		return containers.size() - 1;
	}
	static void destroyPool(unsigned int index) {
		containers.erase(containers.begin() + index);
	}

	static void create(CEntity* entity, CComponentManager* manager) {
		create(entity, manager->getPool<T>());
	}

	static void create(T comp, CEntity* entity, CComponentManager* manager) {
		create(comp, entity, manager->getPool<T>());
	}

	static void create(CEntity* entity, std::vector<T> *pool) {
		// Rember the capacity before the push_back.
		int cap = pool->capacity();

		// Create a new component.
		pool->push_back(T());
		T* base = &pool->back();
		base->entity = entity;
		entity->attachComponent<T>(base);

		// Capacity has changed. update all the entities.
		if(cap != (int)pool->capacity()) {
			for(unsigned int i = 0; i < pool->size()-1; i++) {
				CComponentBase* comp = static_cast<CComponentBase*>(&(*pool)[i]);
				if(comp->entity) comp->entity->updateComponent(&(*pool)[i]);
			}
		}

		// POOL POINTER! (get it? like pool party? HA!)
		base->pool = pool;
	}

	static void create(T comp, CEntity* entity, std::vector<T> *pool) {
		// Rember the capacity before the push_back.
		int cap = pool->capacity();

		// Create a new component.
		pool->push_back(comp);
		T* base = &pool->back();
		base->entity = entity;
		entity->attachComponent<T>(base);

		// Capacity has changed. update all the entities.
		if(cap != (int)pool->capacity()) {
			for(unsigned int i = 0; i < pool->size()-1; i++) {
				CComponentBase* comp = static_cast<CComponentBase*>(&(*pool)[i]);
				if(comp->entity) comp->entity->updateComponent(&(*pool)[i]);
			}
		}

		// POOL POINTER! (get it? like pool party? HA!)
		base->pool = pool;
	}

	static void destroy(T* comp) {
		std::vector<T> *pool = comp->pool;

		// Initialize a index and assign it to the size of the container.
		unsigned int index = pool->size();

		// Loop through the vector while the comp is not yet found.
		for(unsigned int i = 0; i < pool->size() && index < pool->size(); i++) {
			// If the address of the comp is the same as the one in the container, set index to i.
			if(&(*pool)[i] == comp) index = i;
		}

		// If index is not the initialized value, the comp has been found. Remove it from the vector.
		if(index < pool->size()) pool->erase(pool->begin() + index);
	}

	// This function is silly. It works though.
	void destroy() {
		destroy(static_cast<T*>(this));
	}

	void throwMessage() {
		SDL_Event message;
		SDL_zero(message);
		message.type = (Uint32)INTENT::MESSAGE;
		message.user.data1 = (void*)componentID;
		message.user.data2 = this;
		SDL_PushEvent(&message);
	}

	template<typename H>
	bool isComponent() {
		return this->componentID == H::componentID;
	}


protected:
	CComponent(){};
};

template <typename T>
std::vector<std::vector<T>> CComponent<T>::containers;

template <typename T>
unsigned int  CComponent<T>::componentID = CComponentBase::total_components++;

#endif //_COMPONENT_H_