#ifndef _CCOMPPLAYER_H_
#define _CCOMPPLAYER_H_

#include "CComponent.h"
;

class CCompPlayer : public CComponent<CCompPlayer>{
public:
	CCompPlayer() {
		name = "";
		selectedEntity = NULL;
	}

	std::string name;
	CEntity* selectedEntity;
};

#endif //_CCOMPPLAYER_H_