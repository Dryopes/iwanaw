#include "CSysMovement.h"

#include "CCompPosition.h"
#include "CCompMovement.h"
#include "CCompMapped.h"
#include "CCompMap.h"

#include <limits>
#include <iostream>

//////////// MOOOOOOOOOOOORE IF CLAUSUS~!!1!
void CSysMovement::update(CEntity *entity, float deltaTime) {
	if(entity->hasComponent<CCompPosition>() && entity->hasComponent<CCompMovement>()) {
		updateMovement(entity, deltaTime);
		updateVelocity(entity, deltaTime);
		updatePosition(entity, deltaTime);
	}
}

void CSysMovement::updateMovement(CEntity *entity, float deltaTime) {
	CCompPosition* pos = entity->getComponent<CCompPosition>();
	CCompMovement* mov = entity->getComponent<CCompMovement>();

	if(mov->nextX == pos->getX() && mov->nextY == pos->getY()) return;

	float infinity = std::numeric_limits<float>::infinity();
	if(mov->nextX != infinity && mov->nextY != infinity) {
		setMovement(entity, (double)(mov->nextX) - pos->getX(), (double)(mov->nextY) - pos->getY());
	}
	else if(mov->nextX != infinity) {
		setMovement(entity, (double)(mov->nextX) - pos->getX(), 0);
	}
	else if(mov->nextY != infinity) {
		setMovement(entity, 0, (double)(mov->nextY) - pos->getY());
	}
}

void CSysMovement::setMovement(CEntity* entity, double delta_x, double delta_y) {
	CCompMovement* mov = entity->getComponent<CCompMovement>();
	
	double angle = atan2(delta_y, delta_x);
	if(delta_x != 0) mov->moveX = cos(angle);
	if(delta_y != 0) mov->moveY = sin(angle);
}

void CSysMovement::updateVelocity(CEntity* entity, float deltaTime) {
	CCompPosition* pos = entity->getComponent<CCompPosition>();
	CCompMovement* mov = entity->getComponent<CCompMovement>();

	if(mov->moveX != 0 && mov->moveX != mov->speedX*mov->moveX) {
		if(mov->accelerationX == 0) mov->velocityX = mov->speedX*mov->moveX;
		else {
			int weight = mov->moveX/abs(mov->moveX);
			mov->velocityX += mov->speedX * mov->accelerationX*deltaTime * weight;
			if(mov->velocityX*weight > mov->speedX*mov->moveX*weight) mov->velocityX = mov->speedX*mov->moveX;
		}
	}
	else if(mov->moveX == 0 && mov->velocityX != 0) {
		if(mov->deaccelerationX == 0) {
			mov->velocityX = 0;
			mov->nextX = std::numeric_limits<float>::infinity();
			std::cout << "mov->nextX is infinity" << std::endl;
		}
		else {
			mov->velocityX -= mov->speedX * mov->deaccelerationX*deltaTime * mov->velocityX/abs(mov->velocityX);
			if(abs(mov->velocityX) < mov->speedX * mov->deaccelerationX*deltaTime) {
				mov->velocityX = 0;
				mov->nextX = std::numeric_limits<float>::infinity();
			}
		}
	}
//	else if(mov->moveX == 0 && mov->velocityX == 0) mov->nextX = std::numeric_limits<float>::infinity();

	if(mov->moveY != 0 && mov->moveY != mov->speedY*mov->moveY) {
		if(mov->accelerationY == 0) mov->velocityY = mov->speedY*mov->moveY;
		else {
			int weight = mov->moveY/abs(mov->moveY);
			mov->velocityY += mov->speedY * mov->accelerationY*deltaTime * weight;
			if(mov->velocityY*weight > mov->speedY*mov->moveY*weight) mov->velocityY = mov->speedY*mov->moveY;
		} 
	}
	else if(mov->moveY == 0 && mov->velocityY != 0) {
		if(mov->deaccelerationY == 0) {
			mov->velocityY = 0;
			mov->nextY = std::numeric_limits<float>::infinity();
			std::cout << "mov->nextY = infinity" << std::endl;
		}
		else {
			mov->velocityY -= mov->speedY * mov->deaccelerationY*deltaTime * mov->velocityY/abs(mov->velocityY);
			if(abs(mov->velocityY) < mov->speedY * mov->deaccelerationY*deltaTime) {
				mov->velocityY = 0;
				mov->nextY = std::numeric_limits<float>::infinity();
			}
		}
	}
	//else if(mov->moveY == 0 && mov->velocityY == 0) mov->nextY = std::numeric_limits<float>::infinity();
}

void CSysMovement::updatePosition(CEntity* entity, float deltaTime) {
	CCompPosition* pos = entity->getComponent<CCompPosition>();
	CCompMovement* mov = entity->getComponent<CCompMovement>();
	float infinity = std::numeric_limits<float>::infinity();

	if(mov->velocityX != 0) {
		if(mov->nextX != infinity && abs(mov->nextX - pos->getX()) < abs(mov->velocityX * deltaTime)) {
			pos->setX(mov->nextX);
			mov->moveX = 0;
		} else pos->setX(pos->getX() + mov->velocityX * deltaTime);
	}
	if(mov->velocityY != 0){
		if(mov->nextY != infinity && abs(mov->nextY - pos->getY()) < abs(mov->velocityY * deltaTime)) {
			pos->setY(mov->nextY);
			mov->moveY = 0;
		} else pos->setY(pos->getY() + mov->velocityY * deltaTime);
	}

	//std::cout << "nextX: " << mov->nextX << ", velocityX: " << mov->velocityX << ", moveX: " << mov->moveX << std::endl;
	//std::cout << "nextY: " << mov->nextY << ", velocityY: " << mov->velocityY << ", moveY: " << mov->moveY << std::endl << std::endl;
}