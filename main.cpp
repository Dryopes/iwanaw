#include "CSysRender.h"
#include "CSysMovement.h"
#include "CSysRenderMap.h"
#include "CSysInput.h"
#include "CSysMessaging.h"
#include "CSysPath.h"

#include "CCompActor.h"
#include "CCompPosition.h"
#include "CCompMovement.h"
#include "CCompMap.h"
#include "CCompCamera.h"
#include "CCompCameraControlled.h"
#include "CCompInput.h"
#include "CCompMapped.h"
#include "CCompTile.h"
#include "CCompCollision.h"
#include "CCompPlayer.h"
#include "CCompPlayerControlled.h"
#include "CCompPointer.h"
#include "CCompMessage.h"
#include "CCompPath.h"

#include "CRectangle.h"
#include "CHexagon.h"

#include "SDLRenderer.h"
#include "SDLEvent.h"

#include <iostream>
#include <sstream>

#include <SDL_image.h>

bool _looping = true;

void close() {
	_looping = false;
}

void eventing(SDL_Event* handle) {
	if(handle->type == SDL_WINDOWEVENT) {
		switch(handle->window.event) {
		case SDL_WINDOWEVENT_CLOSE: close(); break;
		}
	}
}

int main(int argc, char* args[])
{
	CComponentManager compManager;

	CEntity entity;
	CEntity map;
	CEntity camera;
	CEntity tile;
	CEntity player;
	CEntity pointer;
	CEntity shapetest;

	CCompActor::create(&entity, &compManager);
	CCompMapped::create(&entity, &compManager);
	CCompPosition::create(&entity, &compManager);
	CCompMovement::create(&entity, &compManager);
	CCompCollision::create(&entity, &compManager);
	CCompPlayerControlled::create(&entity, &compManager);
	CCompMessage::create(&entity, &compManager);
	CCompPath::create(&entity, &compManager);

	CCompPosition::create(&camera, &compManager);
	CCompMovement::create(&camera, &compManager);
	CCompCamera::create(&camera, &compManager);
	CCompInput::create(&camera, &compManager);

	CCompPosition::create(&tile, &compManager);
	CCompActor::create(&tile, &compManager);
	CCompTile::create(&tile, &compManager);
	CCompMessage::create(&tile, &compManager);

	CCompPointer::create(&pointer, &compManager);
	CCompPosition::create(&pointer, &compManager);
	CCompMovement::create(&pointer, &compManager);
	CCompPlayerControlled::create(&pointer, &compManager);

	CCompPosition::create(&shapetest, &compManager);
	CCompActor::create(&shapetest, &compManager);

	CCompPlayer::create(&player, &compManager);

	entity.getComponent<CCompPosition>()->setPos(30, 32);

	entity.getComponent<CCompMovement>()->speedX = 100;
	entity.getComponent<CCompMovement>()->speedY = 100;

	entity.getComponent<CCompCollision>()->setShape(new CRectangle(32, 32));
	entity.getComponent<CCompPlayerControlled>()->player = &player;

	entity.getComponent<CCompMapped>()->setTileSpeed(3);

	camera.getComponent<CCompMovement>()->speedX = 500.0f;
	camera.getComponent<CCompMovement>()->speedY = 500.0f;

	camera.getComponent<CCompMovement>()->accelerationX = 5.00f;
	camera.getComponent<CCompMovement>()->accelerationY = 5.00f;

	camera.getComponent<CCompMovement>()->deaccelerationX = 5.00f;
	camera.getComponent<CCompMovement>()->deaccelerationY = 5.00f;

	pointer.getComponent<CCompPlayerControlled>()->player = &player;

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0){
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDLWindow window("Testing etc.", SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED, 800, 600, SDL_WINDOW_SHOWN);

	if(!window.isCreated()) {
		std::cout << "SDL Window could not be created! SDL Error:"
			<< SDL_GetError() << std::endl;
	}

	SDLRenderer renderer(&window, -1, SDL_RENDERER_ACCELERATED );

	if(!renderer.isCreated()) {
		std::cout << "SDL Renderer could not be created! SDL Error:"
			<< SDL_GetError() << std::endl;
	}
	


	CSysRender render(&renderer);
	CSysRenderMap maprender(&render);

	CSysMovement move;
	CSysInput input;
	CSysMessaging message;
	CSysPath path;

	SDL_Surface* test_surf = IMG_Load("resources//test.png");
	SDL_Surface* test_hover_surf = IMG_Load("resources//test_hover.png");
	SDL_Surface* test_select_surf = IMG_Load("resources//test_select.png");
	SDL_Surface* test_select_hover_surf = IMG_Load("resources//test_select_hover.png");

	SDL_Surface* hex = IMG_Load("resources//hexagon.png");
	SDL_Surface* hex_hover = IMG_Load("resources//hexagon_hover.png");

	window.setIcon(test_surf);

	SDL_Texture* test_tex = SDL_CreateTextureFromSurface(renderer.get(), test_surf);
	SDL_Texture* test_hover_tex = SDL_CreateTextureFromSurface(renderer.get(), test_hover_surf);
	SDL_Texture* test_select_tex = SDL_CreateTextureFromSurface(renderer.get(), test_select_surf);
	SDL_Texture* test_select_hover_tex = SDL_CreateTextureFromSurface(renderer.get(), test_select_hover_surf);

	SDL_Texture* hex_tex = SDL_CreateTextureFromSurface(renderer.get(), hex);
	SDL_Texture* hex_hover_tex = SDL_CreateTextureFromSurface(renderer.get(), hex_hover);

	CHexagon hexagon(32);
	hexagon.setOrigin(Point_d(128, 128));
	SDL_Texture* shape_tex = hexagon.toTextureFilled(renderer.get());

	shapetest.getComponent<CCompPosition>()->setPos(270, 256);
	shapetest.getComponent<CCompActor>()->actor.addDefaultTexture(shape_tex);

	entity.getComponent<CCompActor>()->actor.addDefaultTexture(test_tex);
	entity.getComponent<CCompActor>()->actor.addTexture("hover", test_hover_tex);
	entity.getComponent<CCompActor>()->actor.addTexture("select", test_select_tex);
	CTaggedTexture tagged(test_select_hover_tex);
	tagged.addTag(std::string("select"));
	tagged.addTag(std::string("hover"));
	entity.getComponent<CCompActor>()->actor.addTexture(tagged);

	tile.getComponent<CCompActor>()->actor.addDefaultTexture(hex_tex);
	tile.getComponent<CCompActor>()->actor.addTexture("hover", hex_hover_tex);

	tile.getComponent<CCompTile>()->cost = 1;

	CCompMap::create(&compManager, &map, &tile, 60, 80, 32);
	CCompInput::create(&map, &compManager);
	CCompCameraControlled::create(&map, &compManager);

	map.getComponent<CCompCameraControlled>()->camera = &camera;
	entity.getComponent<CCompMapped>()->setMap(&map);

	SDL_FreeSurface(test_surf);
	SDL_FreeSurface(test_hover_surf);
	SDL_FreeSurface(hex);
	SDL_FreeSurface(hex_hover);

	renderer.setDrawBlendMode(SDL_BLENDMODE_BLEND);
	
	//std::cin.get();
	std::cout << "Starting the game loop..." << std::endl;
	int x = 0;
	int begin = SDL_GetTicks();
	int begin_update = SDL_GetTicks();
	unsigned int interval_update = 50;
	double interval = 1000.0f/interval_update;
	while(_looping) {
		x++;
		if((SDL_GetTicks() - begin) >= 1000) { 
			std::stringstream stream;
			stream << "FPS: " << x;
			window.setTitle(stream.str());
			x = 0;
			begin = SDL_GetTicks();
		}
		// event handling part of the loop.
		SDLEvent eventHandler;
		while(eventHandler.poll()) {
			if(eventHandler.get()->type >= SDL_USEREVENT) {
				if(eventHandler.get()->type == (Uint32)INTENT::MESSAGE) {
					message.update(eventHandler.get(), &compManager);
				}
				else {
					input.update(&pointer, eventHandler.get());
					input.update(&camera, eventHandler.get());
					input.update(&map, eventHandler.get());
					input.update(&entity, eventHandler.get());
				}
			}
			else {
			
				eventing(eventHandler.get());
				eventHandler.toIntent();
			}
			
		}


		if(SDL_GetTicks() - begin_update >= interval) {
			begin_update = SDL_GetTicks();
			path.update(&entity);
			move.update(&entity, interval/1000.0f);
			move.update(&camera, interval/1000.0f);
		}

		// Render part of the loop.
		renderer.setDrawColor(255, 255, 255, 255);
		renderer.clear();
		
		maprender.update(&map);
		render.update(&shapetest);
		//hexagon.drawFilled(renderer.get());

		renderer.present();
		
	}
	
	//Quit SDL
	SDL_Quit();
	
	return 0;    
}