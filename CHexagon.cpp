#include "CHexagon.h"
#include "CRectangle.h"

#include "SDL2_gfxPrimitives.h"

#include <cmath>

CHexagon::CHexagon(double radius) {
	this->radius = radius;
}

CHexagon::CHexagon(Point_d origin, double radius) 
	: CShape(origin) {
	this->radius = radius;
}

CHexagon::~CHexagon() {

}

CRectangle CHexagon::getAABB() {
	return CRectangle(origin, this->radius*sqrt(3), this->radius*2);  
}

bool CHexagon::inside(Point_d point) {
	int32_t diffX = abs(point.getX() - origin.getX());         
    int32_t diffY = abs(point.getY() - origin.getY());

	int32_t height = (float)this->radius/2 * sqrt(3.f);

    
	if (diffX > height || diffY > this->radius) return false;

    return this->radius*height - this->radius/2*diffX - height*diffY >= 0;   
}

bool CHexagon::collision(CHexagon* hex) {
	return false;
}

void CHexagon::draw(SDL_Renderer* renderer, bool ignore) {
	Uint8 r, g, b, a;
	SDL_GetRenderDrawColor(renderer, &r, &g, &b, &a);

	Sint16 x[7];
	Sint16 y[7];

	for(Uint8 i = 0; i < 6; ++i) {
		float angle = 2.f * M_PI / 6.f * ((float)i + .5f);
		x[i] = (float)radius * cos(angle) + (ignore ? this->radius*sqrt(3)/2 : origin.getX());
		y[i] = (float)radius * sin(angle) + (ignore ? this->radius : origin.getY());
	}
	x[6] = x[0];
	y[6] = y[0];

	aapolygonRGBA(renderer, x, y, 7, r, g, b, a);
}

void CHexagon::drawFilled(SDL_Renderer* renderer, bool ignore) {
	Uint8 r, g, b, a;
	SDL_GetRenderDrawColor(renderer, &r, &g, &b, &a);

	Sint16 x[7];
	Sint16 y[7];

	for(Uint8 i = 0; i < 6; ++i) {
		float angle = 2.f * M_PI / 6.f * ((float)i + .5f);
		x[i] = (float)radius * cos(angle) + (ignore ? this->radius*sqrt(3)/2 : origin.getX());
		y[i] = (float)radius * sin(angle) + (ignore ? this->radius : origin.getY());
	}
	x[6] = x[0];
	y[6] = y[0];

	filledPolygonRGBA(renderer, x, y, 7, r, g, b, a);
}