#ifndef _CPOINT_IMP_H_
#define _CPOINT_IMP_H_

template<typename T>
CPoint<T>::CPoint() {
	this->x = static_cast<T>(0);
	this->y = static_cast<T>(0);
};

template<typename T>
CPoint<T>::CPoint(T x, T y) {
	this->x = x;
	this->y = y;
};

template<typename T>
CPoint<T>::CPoint(SDL_Point &point) {
	this->x = static_cast<T>(point.x);
	this->y = static_cast<T>(point.y);
};

template<typename T>
void CPoint<T>::setPosition(T x, T y) {
	this->x = x;
	this->y = y;
};

template<typename T>
void CPoint<T>::setX(T x) {
	this->x = x;
};

template<typename T>
void CPoint<T>::setY(T y) {
	this->y = y;
};

template<typename T>
T CPoint<T>::getX() {
	return this->x;
};

template<typename T>
T CPoint<T>::getY() {
	return this->y;
};


#endif //_CPOINT_IMP_H_