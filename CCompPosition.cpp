#include "CCompPosition.h"
#include "CCompMapped.h"

CCompPosition::CCompPosition() {
	setPos(0.0f, 0.0f);
}

CCompPosition::CCompPosition(double x, double y) {
	setPos(x, y);
}

void CCompPosition::setX(double x){
	this->setPos(x, this->y);
}
void CCompPosition::setY(double y){
	this->setPos(this->x, y);
}
void CCompPosition::setPos(double x, double y){
	this->x = x; 
	this->y = y;
	throwMessage();
}
void CCompPosition::updateMapped() {
	if(entity != NULL && this->entity->hasComponent<CCompMapped>()) {
		this->entity->getComponent<CCompMapped>()->updateTilePos();
	}
}
double CCompPosition::getX(){
	return this->x;
}
double CCompPosition::getY(){
	return this->y;
}

Point_d CCompPosition::getPos() {
	return Point_d(this->x, this->y);
}