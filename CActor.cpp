#include "CActor.h"

#include "extra_std.h"

//////////////////////////////////////////////
//											//
//			CTaggedTexture Start			//
//											//
//////////////////////////////////////////////

CTaggedTexture::CTaggedTexture(SDL_Texture* tex) {
	this->texture = tex;
}

CTaggedTexture::CTaggedTexture(SDL_Texture* tex, std::set<std::string> &set) {
	this->texture = tex;
	this->tags = set;
}

CTaggedTexture::~CTaggedTexture() {

}

SDL_Texture* CTaggedTexture::getTexture() {
	return this->texture;
}

void CTaggedTexture::addTag(std::string& tag) {
	this->tags.insert(tag);
}

void CTaggedTexture::addTags(std::vector<std::string>& tags) {
	this->tags.insert(tags.begin(), tags.end());
}

uint16_t CTaggedTexture::countTags() {
	return this->tags.size();
}

bool CTaggedTexture::hasTag(std::string &tag) {
	return std::find(this->tags.begin(), this->tags.end(), tag) != this->tags.end();
}

bool CTaggedTexture::sameTags(std::set<std::string> &tags) {
	return this->tags == tags;
}

//////////////////////////////////////////////
//											//
//			CTaggedTexture End				//
//											//
//////////////////////////////////////////////

//////////////////////////////////////////////
//											//
//				CActor Start				//
//											//
//////////////////////////////////////////////

CActor::CActor() {
	this->selected = NULL;
	this->previous = NULL;
	this->defaultTexture = NULL;

	this->scalex = 1.0f;
	this->scaley = 1.0f;

	this->tilePos.x = 0;
	this->tilePos.y = 0;
	this->tilePos.w = 0;
	this->tilePos.h = 0;
}

CActor::~CActor() {

}

void CActor::addTexture(CTaggedTexture& tex) {
	this->textures.push_back(tex);
}

void CActor::addTexture(std::string name, SDL_Texture *tex) {
	this->textures.push_back(CTaggedTexture(tex));
	this->textures.back().addTag(name);
}

void CActor::addTexture(std::vector<std::string> tags, SDL_Texture *tex) {
	this->textures.push_back(CTaggedTexture(tex));
	this->textures.back().addTags(tags);
}

void CActor::addDefaultTexture(SDL_Texture* tex) {
	this-> selected = this->defaultTexture = tex;;
}

void CActor::addTag(std::string name, int16_t weight) {
	this->tags.insert(name);

	CTaggedTexture* taggedtex = findMostSuited();
	SDL_Texture* tex = taggedtex != NULL ? taggedtex->getTexture() : this->defaultTexture;

	this->previous = this->selected;
	this->selected = tex;
}

void CActor::removeTag(std::string name) {
	this->tags.erase(name);
	
	CTaggedTexture* taggedtex = findMostSuited();
	SDL_Texture* tex = taggedtex != NULL ? taggedtex->getTexture() : this->defaultTexture;
	
	this->previous = this->selected;
	this->selected = tex;
	
}

void CActor::selectPrevious() {
	SDL_Texture* tmp = this->selected;
	this->selected = this->previous;
	this->previous = tmp;
}

SDL_Texture* CActor::getTexture() {
	return this->selected;
}

void CActor::setScale(float x, float y) {
	this->scalex = x;
	this->scaley = y;
}

void CActor::setTilePos(SDL_Rect &tilepos) {
	this->tilePos = tilepos;
}

float CActor::getScaleX() {
	return this->scalex;
}

float CActor::getScaleY() {
	return this->scaley;
}

SDL_Rect CActor::getTilePos() {
	return this->tilePos;
}

CTaggedTexture* CActor::findMostSuited() {
	/*std::multimap<int16_t, std::string> flippedTags = std::flip_map(this->tags);
	std::vector<std::vector<CTaggedTexture>::iterator> candidats;

	for(std::vector<CTaggedTexture>::iterator it = this->_textures.begin();
		it != this->_textures.end(); it++) {
			if(it->countTags() <= this->tags.size()) candidats.push_back(it);
	}

	for(auto range = flippedTags.equal_range(flippedTags.rbegin()->first); ;
		range = flippedTags.equal_range(range.second->first)) {

		std::vector<uint16_t> candidatsCount(candidats.size(), 0);
		uint16_t max = 0;

		for(auto it = range.first; it != range.second; ++it) {
			for(unsigned int index = 0; index < candidats.size(); index++) {
				if(candidats[index]->hasTag(it->second)) {
					max = std::max(max, ++candidatsCount[index]);
				}
			}
		}
		
		for(unsigned int index = 0; index < candidatsCount.size(); ++index) {
			if(candidatsCount[index] != max) {
				candidats.erase(candidats.begin() + index);
				candidatsCount.erase(candidatsCount.begin() + index);
				--index;
			}
		}
		

		if(range.second == flippedTags.end())
			break;
	}

	return candidats.size() == 1 ? &(*candidats[0]) : NULL; */

	
	CTaggedTexture* result = NULL;
	for(unsigned int index = 0; index < this->textures.size(); ++index) {
		if(this->textures[index].sameTags(tags)) {
			result = &this->textures[index];
			break;
		}
	}

	return result;
}

//////////////////////////////////////////////
//											//
//				CActor End					//
//											//
//////////////////////////////////////////////