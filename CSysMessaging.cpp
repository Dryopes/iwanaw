#include "CSysMessaging.h"
#include "CCompMessage.h"
#include "CCompPosition.h"
#include "CCompMovement.h"
#include "CCompMapped.h"

#include <iostream>

void CSysMessaging::update(SDL_Event* eventHandler, CComponentManager* compManager) {
	for(unsigned int index = 0; index < compManager->getPool<CCompMessage>()->size(); index++) {
		update((*compManager->getPool<CCompMessage>())[index].entity, eventHandler);
	}
}

void CSysMessaging::update(CEntity* entity, SDL_Event* eventHandler) {
	if((int)eventHandler->user.data1 == CCompPosition::componentID) {
		CCompMapped* mapped = entity->getComponent<CCompMapped>();

		if(mapped) {
			mapped->updateTilePos();
		}
	}
}