#include "CSysInput.h"

#include "CCompInput.h"
#include "CCompCamera.h"
#include "CCompMovement.h"
#include "CCompActor.h"
#include "CCompHover.h"
#include "CCompMap.h"
#include "CCompPosition.h"
#include "CCompCollision.h"
#include "CCompCameraControlled.h"
#include "CCompMapped.h"
#include "CCompPlayerControlled.h"
#include "CCompPlayer.h"
#include "CCompPointer.h"

#include <iostream>


void CSysInput::update(CEntity *entity, SDL_Event* eventHandler) {

	if(eventHandler->type >= SDL_USEREVENT) {
		if(eventHandler->user.type == (Uint32)INTENT::MOVECAMERA){
			inputMoveCamera(entity, eventHandler);
		}
		if(eventHandler->user.type == (Uint32)INTENT::STOPCAMERA) {
			inputStopCamera(entity, eventHandler);
		}
		if(eventHandler->user.type == (Uint32)INTENT::HOVER) {
			inputHover(entity, eventHandler);
			if(entity->hasComponent<CCompMap>()) {
				inputMap(entity, eventHandler);
			}
		}
		if(eventHandler->user.type == (Uint32)INTENT::POINTER) {
			inputPointer(entity, eventHandler);
		}
	}
}