#include "CSysPath.h"
#include "CCompPath.h"
#include "CCompMovement.h"
#include "CCompMapped.h"

#include <iostream>

void CSysPath::update(CEntity* entity) {
	CCompPath* path = entity->getComponent<CCompPath>();
	CCompMapped* mapped = entity->getComponent<CCompMapped>();
	if(path) {
		CCompMovement* mov = entity->getComponent<CCompMovement>();
		if(path->path.empty()) { 
			std::cout << "Entity path detachted" << std::endl;
			entity->dettachComponent<CCompPath>();
		}
		else {
			float infinity = std::numeric_limits<float>::infinity();
			if(mov->nextX == infinity && mov->nextY == infinity) {
				mov->nextX = path->path[0].getX();
				mov->nextY = path->path[0].getY();
				std::cout << "Path[0] (" << path->path[0].getX() << ", " << 
					path->path[0].getY() << ") erased. " << std::endl;
				path->path.erase(path->path.begin());
				
			}

		}		
	}
}