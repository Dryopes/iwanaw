#ifndef _CSYSMESSAGING_H_
#define _CSYSMESSAGING_H_

#include "CSystem.h"
#include "CComponentManager.h"
#include <sdl.h>

class CSysMessaging : public CSystem {
public:
	void update(SDL_Event*, CComponentManager*);
	void update(CEntity*, SDL_Event*);
};

#endif