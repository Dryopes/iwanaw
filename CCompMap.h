#ifndef _CCOMPMAP_H_
#define _CCOMPMAP_H_

#include "CComponent.h"
#include "CShape.h"

#include <sdl.h>

class CCompMap : public CComponent<CCompMap> {
public:
	static void create(CComponentManager* manager, CEntity* entity, SDL_Texture* tex, unsigned int width, unsigned int height, unsigned int radius);
	static void create(CComponentManager* manager, CEntity* entity, CEntity* tile, unsigned int width, unsigned int height, unsigned int radius);

	std::vector<CEntity> tiles;
	unsigned int radius;
	unsigned int width, height;

public:
	unsigned int getTile(double x, double y);
	Point_i32 getTileCoords(double x, double y);
	Point_i32 getTileCoordsMin(double x, double y);
	Point_i32 getTileCoordsMax(double x, double y);
	Point_d getTilePos(int tile);
};

#endif