#ifndef _SDLEVENT_H_
#define _SDLEVENT_H_

#include <iostream>
#include <SDL.h>

#include "Intent.h"

class SDLEvent {
public:
	SDLEvent();
	~SDLEvent();

	bool poll();
	void toIntent();

	SDL_Event* get();

private:
	SDL_Event handler;
};

#endif