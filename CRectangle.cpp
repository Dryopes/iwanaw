#include "CRectangle.h"

#include "extra_std.h"

CRectangle::CRectangle(double width, double height) {
	this->width = width;
	this->height = height;
}

CRectangle::CRectangle(Point_d origin, double width, double height)
	: CShape(origin)
{
	this->width = width;
	this->height = height;
}

CRectangle CRectangle::getAABB() {
	return *this;
}

void CRectangle::setTopLeft(Point_d topleft) {
	this->origin = Point_d(topleft.getX() + width/2, topleft.getY() + height/2);
}

double CRectangle::getWidth() {
	return width;
}

double CRectangle::getHeight() {
	return this->height;
}

Point_d CRectangle::getTopLeft() {
	return Point_d(this->origin.getX() - this->width/2, this->origin.getY() - this->height/2);
}

Point_d CRectangle::getTopRight(){
	return Point_d(this->origin.getX() + this->width/2, this->origin.getY() - this->height/2);
}

Point_d CRectangle::getBottomLeft(){
	return Point_d(this->origin.getX() - this->width/2, this->origin.getY() + this->height/2);
}

Point_d CRectangle::getBottomRight(){
	return Point_d(this->origin.getX() + this->width/2, this->origin.getY() + this->height/2);
}

bool CRectangle::inside(Point_d point) {
	return point.getX() >= (this->origin.getX() - this->width/2) &&
		point.getY() >= (this->origin.getY() - this->height/2) &&
		point.getX() <= (this->origin.getX() + this->width/2) &&
		point.getY() <= (this->origin.getY() + this->height/2);
}

void CRectangle::draw(SDL_Renderer* r, bool ignore) {
	SDL_Rect rect = {ignore ? 0 : this->origin.getX(), ignore ? 0 : this->origin.getY(), width, height};
	SDL_RenderDrawRect(r, &rect);
}

void CRectangle::drawFilled(SDL_Renderer* r, bool ignore) {
	SDL_Rect rect = {ignore ? 0 : this->origin.getX(), ignore ? 0 : this->origin.getY(), width, height};
	SDL_RenderFillRect(r, &rect);
}
