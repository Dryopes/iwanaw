#ifndef _CSYSRENDERMAP_H_
#define _CSYSRENDERMAP_H_

#include "CSysRender.h"
#include "CCompMap.h"

class CSysRenderMap : public CSystem {
private:
	CSysRender* render;

public:
	CSysRenderMap(CSysRender* render);
	~CSysRenderMap();

	void update(CEntity* map);
	void renderTile(CEntity* tile, long x, long y);
	void renderTileEntities(CEntity* tile, long x, long y);
	void renderGrid(CEntity* map, long x, long y);
};

#endif