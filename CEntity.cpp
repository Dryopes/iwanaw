#include "CEntity.h"

#include "CComponent.h"

#include <iostream>

CEntity::CEntity() {
	this->components.resize(CComponentBase::total_components);
	this->name = "NamelessEntity";
}

CEntity::CEntity(std::string name) {
	this->components.resize(CComponentBase::total_components);
	this->name = name;
}

CEntity::~CEntity() {

}

void CEntity::dettachComponent(size_t type) {
	if(this->components[type]) {
		this->components[type]->destroy();
		this->components[type] = NULL;
	}
}

bool CEntity::hasComponent(size_t type) {
	return this->components.size() > type && this->components[type] != NULL;
}

CComponentBase* CEntity::getComponent(size_t type) {
	return this->components[type];
}

void CEntity::setName(std::string name) {
	this->name = name;
}

std::string CEntity::getName() {
	return this->name;
}