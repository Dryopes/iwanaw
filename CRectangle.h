#ifndef _CRECTANGLE_H_
#define _CRECTANGLE_H_

#include "CShape.h"
;
class CRectangle : public CShape {
public:
	CRectangle(Point_d origin, double width, double height);
	CRectangle(double width, double height);

	void setTopLeft(Point_d topleft);

	CRectangle getAABB();

	double getWidth();
	double getHeight();

	Point_d getTopLeft();
	Point_d getTopRight();
	Point_d getBottomLeft();
	Point_d getBottomRight();

	bool inside(Point_d point);
	//bool collide(CShape* shape);
	bool collide(CRectangle* rect);

	void draw(SDL_Renderer*, bool ignore_origin);
	void drawFilled(SDL_Renderer*, bool ignore_origin);


private:
	double width, height;
};

#endif