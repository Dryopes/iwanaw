#include "CSysRender.h"
#include "CCompPosition.h"
#include "CCompActor.h"

#include <iostream>

CSysRender::CSysRender(SDLRenderer* renderer) {
	this->renderer = renderer;
}

CSysRender::~CSysRender() {

}

SDLRenderer* CSysRender::getRenderer() {
	return this->renderer;
}

void CSysRender::update(CEntity* entity) {
	update(entity, 0, 0);
}

void CSysRender::update(CEntity* entity, long offset_x, long offset_y) {

		CActor* act = &entity->getComponent<CCompActor>()->actor;
		CCompPosition* pos = entity->getComponent<CCompPosition>();

		int width, height;
		SDL_QueryTexture(act->getTexture(), NULL, NULL, &width, &height);
		width = (int)(width * act->getScaleX());
		height = (int)(height * act->getScaleY());

		SDL_Rect rect = {pos->getX() - width/2 - offset_x, pos->getY() - height/2 - offset_y, 
			width, height};
		SDL_Rect viewport = this->renderer->getViewport();

		if(!(rect.x + width <= viewport.x || rect.y + height <= viewport.y
			|| rect.x >= viewport.w || rect.y>= viewport.h))
		{
			if(SDL_RectEmpty(&act->getTilePos())) {
				this->renderer->drawTexture(act->getTexture(), NULL, &rect);
			} else {
				this->renderer->drawTexture(act->getTexture(), &act->getTilePos(), &rect);
			}
		}
}