#ifndef _CCOMPTILE_H_
#define _CCOMPTILE_H_

#include "CComponent.h"

class CCompTile : public CComponent<CCompTile>{
public:
	std::vector<CEntity*> entities;
	float cost;
};

#endif