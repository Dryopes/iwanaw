#ifndef _CHEXAGON_H_
#define _CHEXAGON_H_

#include "CShape.h"

class CHexagon : public CShape {
public:
	CHexagon(double radius);
	CHexagon(Point_d point, double radius);
	~CHexagon();

	CRectangle getAABB();

	bool inside(Point_d point);
	bool collision(CHexagon* hex);

	void draw(SDL_Renderer*, bool ignore_origin);
	void drawFilled(SDL_Renderer*, bool ignore_origin);
private:
	double radius;
};

#endif