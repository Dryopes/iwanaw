#ifndef _CACTOR_H_
#define _CACTOR_H_

#include <string>
#include <unordered_map>
#include <vector>
#include <set>

#include <SDL.h>

class CTaggedTexture {
public:
	CTaggedTexture(SDL_Texture* tex);
	CTaggedTexture(SDL_Texture* tex, std::set<std::string> &set);
	~CTaggedTexture();

	SDL_Texture* getTexture();

	void addTag(std::string &tag);
	void addTags(std::vector<std::string> &tags);
	uint16_t countTags();
	bool hasTag(std::string &tag);
	bool sameTags(std::set<std::string> &tags);

private:
	std::set<std::string> tags;
	SDL_Texture* texture;
};

class CActor{
public:
	CActor();
	~CActor();

	void addTag(std::string name, int16_t weight = 0);
	void removeTag(std::string name);

	void addDefaultTexture(SDL_Texture*);
	void addTexture(CTaggedTexture &texture);
	void addTexture(std::string, SDL_Texture*);
	void addTexture(std::vector<std::string>, SDL_Texture*);
	void selectPrevious();

	SDL_Texture* getTexture();

	void setScale(float x, float y);
	void setTilePos(SDL_Rect &tilepos);

	float getScaleX();
	float getScaleY();

	SDL_Rect getTilePos();

private:
	SDL_Texture* findTexture();
	CTaggedTexture* findMostSuited();

private:
	std::vector<CTaggedTexture> textures;
	SDL_Texture* defaultTexture;
	std::set<std::string> tags;

	SDL_Texture* selected;
	SDL_Texture* previous;

	float scalex, scaley;
	SDL_Rect tilePos;

};

#endif //_CACTOR_H_