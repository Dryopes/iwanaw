#include "CCompMapped.h"
#include "CCompMap.h"
#include "CCompTile.h"

#include <limits>
#include <iostream>

Paths::Paths() {

}

Paths::Paths(std::vector<std::vector<unsigned int>> c) {
	this->container = c;
}

std::vector<unsigned int> Paths::getPositions() {
	std::vector<unsigned int> result;

	for(unsigned int index = 0; index < container.size(); ++index) {
	result.push_back(container[index].back());
	}	

	return result;
}

std::vector<unsigned int> Paths::getPath(unsigned int tile) {
	std::vector<unsigned int> result;

	for(unsigned int index = 0; index < container.size(); ++index) {
		if(container[index].back() == tile) result = container[index];
	}	

	return result;
}



CCompMapped::CCompMapped() {
	this->map = NULL;
	this->tilePos = std::numeric_limits<unsigned int>::max();
}

void CCompMapped::setMap(CEntity* map) {
	this->map = map;
	updateTilePos();
	throwMessage();
}

void CCompMapped::setTilePos(unsigned int tilePos) {
	if(this->tilePos != tilePos) throwMessage();
	this->tilePos = tilePos;
}

void CCompMapped::setTileSpeed(double tileSpeed) {
	this->tileSpeed = tileSpeed;
	throwMessage();
}

void CCompMapped::setPaths(Paths paths) {
	this->paths = paths;
}

void CCompMapped::removePaths() {
	this->paths = Paths();
}

CEntity* CCompMapped::getMap() {
	return this->map;
}

unsigned int CCompMapped::getTilePos() {
	return this->tilePos;
}

double CCompMapped::getTileSpeed() {
	return this->tileSpeed;
}

Paths CCompMapped::getPaths() {
	return this->paths;
}

void CCompMapped::updateTilePos() {
	if(this->map == NULL)
		return;

	CCompPosition* comppos = this->entity->getComponent<CCompPosition>();
	CCompMap* compmap = this->map->getComponent<CCompMap>();

	unsigned int oldpos = this->tilePos;
	this->tilePos = compmap->getTile(comppos->getX(), comppos->getY());

	if(oldpos != tilePos) {
		CCompTile* newTile = compmap->tiles[this->tilePos].getComponent<CCompTile>();

		if(oldpos != std::numeric_limits<unsigned int>::max()) {
			CCompTile* oldTile = compmap->tiles[oldpos].getComponent<CCompTile>();

			std::vector<CEntity*>::iterator iterator;
			iterator = std::find(oldTile->entities.begin(), oldTile->entities.end(), this->entity);

			if(iterator != oldTile->entities.end())
				oldTile->entities.erase(iterator);
		}
		newTile->entities.push_back(this->entity);		
	}
}