#ifndef _CPOINT_H_
#define _CPOINT_H_

#include <SDL.h>

template<typename T>
class CPoint {
public:
	CPoint();
	CPoint(T x, T y);
	CPoint(SDL_Point &point);

	void setPosition(T x, T y);
	void setX(T x);
	void setY(T y);

	T getX();
	T getY();

private:
	T x;
	T y;
};
#include "CPoint_imp.h"


typedef CPoint<uint8_t> Point_ui8;
typedef CPoint<uint16_t> Point_ui16;
typedef CPoint<uint32_t> Point_ui32;
typedef CPoint<uint64_t> Point_ui64;

typedef CPoint<int8_t> Point_i8;
typedef CPoint<int16_t> Point_i16;
typedef CPoint<int32_t> Point_i32;
typedef CPoint<int64_t> Point_i64;

typedef CPoint<float> Point_f;
typedef CPoint<double> Point_d;

#endif