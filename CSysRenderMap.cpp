#include "CSysRenderMap.h"
#include "CCompPosition.h"
#include "CCompCameraControlled.h"
#include "CCompTile.h"

#include <iostream>
#include <cmath>

CSysRenderMap::CSysRenderMap(CSysRender* render) {
	this->render = render;
}

CSysRenderMap::~CSysRenderMap() {

}

void CSysRenderMap::update(CEntity* map) {
	CCompMap* compmap = map->getComponent<CCompMap>();
	CCompPosition* compcam = map->getComponent<CCompCameraControlled>()->camera->getComponent<CCompPosition>();

	SDL_Rect viewport = render->getRenderer()->getViewport();

	Point_i32 startTile = compmap->getTileCoordsMin(compcam->getX(), compcam->getY());
	Point_i32 endTile = compmap->getTileCoordsMax(compcam->getX() + viewport.w, compcam->getY() + viewport.h);

	int startX = startTile.getX();
	int startY = startTile.getY();

	int endX = endTile.getX();
	int endY = endTile.getY();

	//std::cout << "pixel start: \t(" << compcam->getX() << ", " << compcam->getY() << ") \t\t pixel end: \t(" << compcam->getX() + viewport.w << ", " << compcam->getY() + viewport.h << ")" << std::endl;
	//std::cout << "start: \t\t(" << startX << ", " << startY << ")\t end: \t\t(" << endX << ", " << endY << ")" << std::endl << std::endl;

	if(startX < 0) startX = 0;
	if(startY < 0) startY = 0;
	if(endX > compmap->width-1) endX = compmap->width-1;
	if(endY > compmap->height-1) endY = compmap->height-1;

	for(unsigned int y = startY; y <= endY; y++) {
		for(unsigned int x = startX; x <= endX; x++) {
			unsigned int tile = x + y*compmap->width;
			renderTile(&compmap->tiles[tile], compcam->getX(), compcam->getY());
		}
	}

	renderGrid(map, 0, 0);

	for(unsigned int y = startY; y <= endY; y++) {
		for(unsigned int x = startX; x <= endX; x++) {
			unsigned int tile = x + y*compmap->width;
			renderTileEntities(&compmap->tiles[tile], compcam->getX(), compcam->getY());
		}
	}

}

void CSysRenderMap::renderTile(CEntity* tile, long x, long y) {
	render->update(tile, x, y);
}

void CSysRenderMap::renderTileEntities(CEntity* tile, long x, long y) {
	CCompTile* comp = tile->getComponent<CCompTile>();
	
	for(CEntity* ent : comp->entities) {
		render->update(ent, x, y);
	}
}

void CSysRenderMap::renderGrid(CEntity* map, long x, long y) {
	CCompMap* compmap = map->getComponent<CCompMap>();
	CCompPosition* compcam = map->getComponent<CCompCameraControlled>()->camera->getComponent<CCompPosition>();

	render->getRenderer()->setDrawColor(255, 255, 0, 255);

	int width = compmap->radius*sqrt(3);
	int ymove = compmap->radius*1.5;
	int ymodulo = compmap->radius*3;
	for(unsigned int i = 0; i < 18; i++) {
		for(unsigned int j = 0; j < 16; j++) {
			SDL_Point point;
			point.x = width/2 + i*width - (int)compcam->getX()%(width*2);
			point.y = compmap->radius + j*ymove - (int)compcam->getY()%ymodulo;
			if(j%2 == 1) point.x += width/2;

			render->getRenderer()->drawHexagon(point, compmap->radius, false);
		}
	}
}