#include "CCompMap.h"
#include "CCompTile.h"
#include "CCompActor.h"
#include "CCompPosition.h"

#include <iostream>
#include <cmath>

void CCompMap::create(CComponentManager* manager, CEntity* entity, SDL_Texture* tex, unsigned int width, unsigned int height, unsigned int radius) {
	CComponent<CCompMap>::create(entity, manager);
	CCompMap* map = entity->getComponent<CCompMap>();
	map->width = width;
	map->height = height;
	map->radius = radius;
	int tilewidth = radius*sqrt(3);

	map->tiles.resize(width*height);

	for(unsigned int y = 0; y < height; y++) {
		for(unsigned int x = 0; x < width; x++) {
			CEntity* tile = &map->tiles[x + y*width];
			CCompTile::create(tile, manager);
			CCompActor::create(tile, manager);
			CCompPosition::create(tile, manager);

			CCompActor* act = tile->getComponent<CCompActor>();
			CCompPosition* pos = tile->getComponent<CCompPosition>();

			act->actor.addDefaultTexture(tex);

			pos->setY(y*radius*1.5f);
			pos->setX(x*tilewidth);

			if(y % 2 != 0) pos->setX(pos->getX() + tilewidth/2.f);
		}
	}
}

void CCompMap::create(CComponentManager* manager, CEntity* entity, CEntity* example_tile, unsigned int width, unsigned int height, unsigned int radius) {
	CComponent<CCompMap>::create(entity, manager);
	CCompMap* map = entity->getComponent<CCompMap>();
	map->width = width;
	map->height = height;
	map->radius = radius;
	int tilewidth = radius*sqrt(3);

	map->tiles.resize(width*height);

	for(unsigned int y = 0; y < height; y++) {
		for(unsigned int x = 0; x < width; x++) {
			CEntity* tile = &map->tiles[x + y*width];
			CCompTile::create(*example_tile->getComponent<CCompTile>(), tile, manager);
			CCompActor::create(*example_tile->getComponent<CCompActor>(), tile, manager);
			CCompPosition::create(*example_tile->getComponent<CCompPosition>(), tile, manager);

			CCompPosition* pos = tile->getComponent<CCompPosition>();

			pos->setY(radius + y*radius*1.5f);
			pos->setX(tilewidth/2 + x*tilewidth);

			if(y % 2 != 0) pos->setX(pos->getX() + tilewidth/2.f);
		}
	}
}

unsigned int CCompMap::getTile(double x, double y) { 
	Point_i32 point = getTileCoords(x,y);
	return point.getX() + point.getY() * this->width;
}

Point_i32 CCompMap::getTileCoords(double x, double y) {
	int side = radius * 3.f / 2.f;
	int height = radius * sqrt(3.f);

	int ci = floor(y / side);
	int cx = y - ci*side;

	int ty = x - (ci%2) * height / 2;
	int cj = floor(ty / height);
	int cy = ty - height*cj;

	if(cx < abs((double)this->radius / 2.f - (double)this->radius * cy / height)) {
		--ci;
		if(ci != 0)			cj -= (ci % 2) * (ci/(abs(ci)));
		if(cy >= height/2)	cj += 1;
	}

	return Point_i32(cj, ci);
}

Point_i32 CCompMap::getTileCoordsMin(double x, double y) {
	double width = radius * sqrt(3.f);
	double diameter = radius * 3.f / 2.f;

	x = x - width/2.f;
	y = y - radius/2.f;

	int result_x = x / width;
	int result_y = y / diameter;

	return Point_i32(result_x, result_y);
}

Point_i32 CCompMap::getTileCoordsMax(double x, double y) {
	double width = radius * sqrt(3.f);
	double diameter = radius * 3.f / 2.f;

	x = x + width / 2.f;
	y = y + radius / 2.f;

	int result_x = x / width;
	int result_y = y / diameter;

	return Point_i32(result_x, result_y);
}


Point_d CCompMap::getTilePos(int tile) {
	int tilewidth = radius*sqrt(3);

	int tile_x = tile % width;
	int tile_y = tile / width;

	double point_x = tile_x * tilewidth		+ tilewidth/2;
	double point_y = tile_y * radius * 1.5f + radius;

	if(tile_y % 2) point_x += tilewidth/2.f;

	//std::cout << "x: " << point_x << ", y: " << point_y << std::endl;

	return Point_d(point_x + 1, point_y + 1);
}